package com.selenium.core.SampleTestNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MethodTestInParallel {
	WebDriver driver;
	 @BeforeMethod
	    public void beforeMethod() {
		 long id = Thread.currentThread().getId();
		  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
		  //System.setProperty("webdriver.gecko.driver","C:\\Users\\sjaltare\\Documents\\geckodriver.exe");
		  driver = new ChromeDriver();
		  System.out.println("beforeMethod Thread id is: " + id);
		
	    }
	 
	    @Test
	    public void OpenAmazonHomePage() {
	        long id = Thread.currentThread().getId();
	        System.out.println("Open Amazon Home Page Method: Thread id is: " + id);
	        driver.get("http://www.amazon.com");
	    }
	 
	    @Test
	    public void OpenGoogleHomePageTest() {
	        long id = Thread.currentThread().getId();
	        System.out.println("Open Google Home Page Method: Thread id is: " + id);
	        driver.get("https://www.google.com");
	    }
	    
	    @Test
	    public void OpenFaceBookHomePageTest() {
	        long id = Thread.currentThread().getId();
	        System.out.println("Open FaceBook Home Page Method: Thread id is: " + id);
	        driver.get("https://www.facebook.com");
	    }
	 
	 
	    /*@AfterMethod
	    public void afterMethod() {
	       // driver.close();
	       // driver.quit();
	      
	    }*/
}

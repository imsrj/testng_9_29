package com.selenium.core.SampleTestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;

public class MultiBrowser {
    WebDriver driver;
    
 @Parameters("browser")
 @BeforeMethod
  public void beforeMethod(String browser) {
	// System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
	
	 if(browser.equalsIgnoreCase("firefox")) {
		 System.setProperty("webdriver.gecko.driver","C:\\Users\\sjaltare\\Documents\\geckodriver.exe");
		  driver = new FirefoxDriver();
	 
	  // If browser is IE, then do this	  
	 
	  }else if (browser.equalsIgnoreCase("ie")) { 
	 
		  // Here I am setting up the path for my IEDriver
	 
		 // System.setProperty("webdriver.ie.driver", "D:\ToolsQA\OnlineStore\drivers\IEDriverServer.exe");
		  System.setProperty("webdriver.ie.driver", "C:\\Users\\sjaltare\\Desktop\\IEDriverServer.exe");
		  driver = new InternetExplorerDriver();
	 
	  } 
	 
	  else if(browser.equalsIgnoreCase("chrome")){
		  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
		  driver = new ChromeDriver();
	  }
  }
  @Test
  public void OpenAmazonHomePageTest() {
	  
	  driver.get("http://www.amazon.com");
}
  
  @Test
  public void OpenGoogleHomePageTest() {
	  
	  driver.get("https://www.google.com");
}
 
  @Test
  public void OpenFaceBookHomePageTest() {
	  
	  driver.get("https://www.facebook.com/");
}
 
  
  
 /*@AfterMethod
  public void afterMethod() {
	  //driver.close();
	 // driver.quit();
  }*/

}
